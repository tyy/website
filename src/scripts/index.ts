// Spin the avatar when clicked because funny
enum SpinLock {
	None,
	Clockwise,
	CounterClockwise
}
const avatarElement = document.getElementById("navbar-avatar");
if (avatarElement) {
	let spinLock: [SpinLock, number] = [SpinLock.None, -1];
	let interval: null | number = null;

	avatarElement.style.cursor = "pointer";
	avatarElement.addEventListener("click", async (e: MouseEvent) => {
		if (
			spinLock[0] ===
			(e.shiftKey ? SpinLock.CounterClockwise : SpinLock.Clockwise)
		)
			return;
		else if (spinLock[0] !== SpinLock.None) {
			// This happens if the current spinlock is going one way, but then is clicked to go the other way
			spinLock = [
				e.shiftKey ? SpinLock.CounterClockwise : SpinLock.Clockwise,
				spinLock[1]
			];
			return;
		} else
			spinLock = e.shiftKey
				? [SpinLock.CounterClockwise, 360 * 2]
				: [SpinLock.Clockwise, 0];
		interval = setInterval(() => {
			avatarElement.style.transform = `rotate(${
				spinLock[0] == SpinLock.CounterClockwise ? --spinLock[1] : ++spinLock[1]
			}deg)`;
			if (
				spinLock[1] === (spinLock[0] == SpinLock.CounterClockwise ? 0 : 360 * 2)
			) {
				if (interval) clearInterval(interval);
				spinLock = [SpinLock.None, -1];
			}
		}, 0);
	});
}

// Konami code b/c why not
const sequence = [
	"ArrowUp",
	"ArrowUp",
	"ArrowDown",
	"ArrowDown",
	"ArrowLeft",
	"ArrowRight",
	"ArrowLeft",
	"ArrowRight",
	"KeyB",
	"KeyA"
];
let stage = 0;
document.addEventListener("keydown", (e: KeyboardEvent) => {
	if (e.code === sequence[stage]) stage++;
	else stage = 0;

	if (stage === sequence.length) {
		// Reset the stage of code
		stage = 0;
		// Check if the hidden element already exists
		const existingElement = document.getElementById(
			"never-gonna-give-you-up"
		) as HTMLAudioElement | null;
		// If it does and it is playing, pause it
		if (existingElement && !existingElement.paused) existingElement.pause();
		// If it exists and it is paused, seek to the start and play it
		else if (existingElement && existingElement.paused) {
			existingElement.fastSeek(0);
			existingElement.play();
		}
		// Otherwise, create the element and play it
		else {
			const element = document.createElement("audio");
			element.setAttribute(
				"src",
				"https://archive.org/serve/never-gonna-give-you-up-original/Never%20Gonna%20Give%20You%20Up%20Original.mp3"
			);
			element.id = "never-gonna-give-you-up";
			element.style.display = "none";
			document.body.appendChild(element);
			element.play();
		}
	}
});
